import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Post from "./components/Post";
import PostsList from "./components/PostsList";
import { useState } from "react";

function App() {
  const [posts, setPosts] = useState([]);

  const addPost = (post) => {
    setPosts([...posts, post]);
  };

  const editPost = (index, updatedPost) => {
    const updatedPosts = [...posts];
    updatedPosts[index] = updatedPost;
    setPosts(updatedPosts);
  };

  const deletePost = (index) => {
    const updatedPosts = [...posts];
    updatedPosts.splice(index, 1);
    setPosts(updatedPosts);
  };

  return (
    <Router> {/* Wrap Routes with Router */}
      <div className="container">
        <h1>Blog-App</h1>
        <Routes>
          <Route
            exact
            path="/"
            element={
              <PostsList
                posts={posts}
                editPost={editPost}
                deletePost={deletePost}
              />
            }
          />
          <Route path="/create" element={<Post addPost={addPost} />} />
          <Route
            path="/edit/:index"
            element={<Post posts={posts} editPost={editPost} />}
          />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
